const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.get('/', (req, res, next) => {
    res.send(`Hola desde express`);
});

app.get('/mi_ruta', (req, res, next) => {
    const { nombre, edad } = req.query;
    res.send(`Hola ${nombre}, tienes ${edad}`);
});

app.get('/mi_parametro/:nombre/:edad', (req, res, next) => {
    const { nombre, edad } = req.params;
    res.send(`Hola ${nombre}, tienes ${edad}`);
});

app.listen(port, () => {
    console.log(`Servidor corriendo en el puerto ${port}`);
});